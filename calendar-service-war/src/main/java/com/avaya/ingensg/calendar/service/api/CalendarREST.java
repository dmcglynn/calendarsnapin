package com.avaya.ingensg.calendar.service.api;

import com.avaya.ingensg.calendar.service.entry.CalendarEntry;
import com.wordnik.swagger.annotations.Api;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

@Stateless
@Path("/calendar")
@Api(value = "/calendar", tags = {"calendar"})
public class CalendarREST {

    private final Map<String, CalendarEntry> entries = new HashMap<>();
    
    public CalendarREST() {
    }

    @OPTIONS
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.MEDIA_TYPE_WILDCARD)
    public Response options() {
        ResponseBuilder resp = Response.ok();
        resp.header("Access-Control-Allow-Origin", "*");
        resp.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        resp.header("Access-Control-Allow-Headers", "x-requested-with,Content-Type");
        resp.entity("");
        return resp.build();
    }

    @GET
    @Consumes(MediaType.WILDCARD)
    @Produces(MediaType.TEXT_HTML)
    @Path("/check")
    public Response check() {
        
        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html><body><div>");
        htmlBuilder.append("<h1>Calendar REST has started</h1>");
        htmlBuilder.append("</div></body></html>");

        return Response.ok()
                .entity(htmlBuilder.toString())
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/entry")
    public Response create(CalendarEntry entry) {
        System.out.println("Received: " + entry);
        entries.put(entry.getLocation()+entry.getService(), entry);
        String response = "Calendar Entry created!";
        System.out.println("Returning response: " + response);
        return Response.ok()
                .entity(response)
                .build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/entry")
    public Response get(@QueryParam("location") String location, @QueryParam("service") String service) {
        CalendarEntry entry = entries.get(location+service);
        return Response.ok()
                .entity(entry)
                .build();
    }
    
}
