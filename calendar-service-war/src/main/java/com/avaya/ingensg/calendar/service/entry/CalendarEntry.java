package com.avaya.ingensg.calendar.service.entry;

/**
 *
 * @author mcglynnd
 */


public class CalendarEntry {
    private String location;
    private String service;
    private String emergencyMode;
        
    public CalendarEntry() {
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getEmergencyMode() {
        return emergencyMode;
    }

    public void setEmergencyMode(String emergencyMode) {
        this.emergencyMode = emergencyMode;
    }

    @Override
    public String toString() {
        return "CalendarEntry{" + "location=" + location + ", service=" + service + ", emergencyMode=" + emergencyMode + '}';
    }

    
}
