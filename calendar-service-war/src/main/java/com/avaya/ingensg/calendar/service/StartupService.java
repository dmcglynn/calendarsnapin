package com.avaya.ingensg.calendar.service;

import com.gigaspaces.management.GigaSpacesRuntime;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Singleton EJB instantiated by the WebSphere container when the war is
 * started.
 */
@Startup
@Singleton
public class StartupService {
    
    private static final Logger LOG = LoggerFactory.getLogger(StartupService.class.getName());
    
//    @Inject
//    private SpaceBasedIdGenerator idGenerator;

    @PostConstruct
    public void init() {
        LOG.info("START init");
//        idGenerator.init();
        LOG.info("END init");
    }

    @PreDestroy
    public void destroy() {
        LOG.info("START destroy");
        try {
            GigaSpacesRuntime.shutdown();
        } catch (Exception ex) {
            System.err.println("Failed GigaSpacesRuntime.shutdown()" + ex);
        }
        LOG.info("END destroy");
    }
}
