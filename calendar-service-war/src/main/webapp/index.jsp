<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>Emergency </title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
     <script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
     <!--<link rel ="stylesheet" href="css/main.css">-->
    <link rel="stylesheet" href=" http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.css" />
    <script type="text/javascript" src="http://code.jquery.com/mobile/1.3.2/jquery.mobile-1.3.2.min.js"></script>
    
    <script type="text/javascript">
        $(document).delegate("#submit", "vclick", function() {
            var location = $("#location").val();
            var service = $("#service").val();
            var mode = $("#flip-1").val();

            var data= {
                "location":location,
                "service":service,
                "emergencyMode":mode
            };

            $.ajax({  
                url:'http://localhost:9080/services/CalendarService/calendar/entry',  
                headers: {
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                },
                type:'post',
                data :  JSON.stringify(data),      
                dataType: 'text',
                success: function(data) { 
                    console.log(data);
                    $('#response-message').append(data);
                },
                error: function(data) {
                    console.log("Error: " + JSON.stringify(data));
                }
            });
        });
    </script>

</head>

<body>
    <div id="main" style="width: 50%; margin: auto">
        <div>
            <strong>Location:</strong>
            <input id="location" type="text" name="location"/><br/>
            <strong>Service:</strong>
            <input id="service" type="text" name="service"/><br/>
        </div>
        <label for="flip-1">Emergency Mode</label>
        <select name="flip-1" id="flip-1" data-role="slider">
            <option value="off">Off</option>
            <option value="on">On</option>
        </select> 
        <button id="submit">Submit</button>

        <p>
            <div id="response-message" style="color: red"></div>
        </p>
    </div>
</body>
</html>
